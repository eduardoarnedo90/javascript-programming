export default function isOdd(num) {
  if ((num % 2) === 0) {
    return 'Even';
  } else {
    return 'Odd';
  }

}