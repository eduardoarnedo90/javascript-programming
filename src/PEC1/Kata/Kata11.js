export default class plantTree {

  constructor(species, fruit) {

    if (typeof species !== 'string' || typeof fruit !== 'string') {
      return null;
    } else {
      this.species = species;
      this.fruit = fruit;
    }

  }

  // species
  get getSpecies() {
    return this.species;
  }

  set setSpecies(species) {
    if (typeof species === 'string') {
      this.species = species;
    }
  }

  // fruit
  get getFruit() {
    return this.fruit;
  }

  set setFruit(fruit) {
    if (typeof fruit === 'string' && fruit !== 12) {
      this.fruit = fruit;
    } else {
      throw new Error();
    }
  }

}