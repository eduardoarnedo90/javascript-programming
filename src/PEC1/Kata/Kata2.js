export default function sumPositiveElements(arrayNumbers) {

  let sum = 0;

  if (arrayNumbers.length === 0) {
    return sum;
  }

  for (let i = 0; i < arrayNumbers.length; i++) {
    if (arrayNumbers[i] > 0) {
      sum += arrayNumbers[i];
    }
  }

  return sum;

}