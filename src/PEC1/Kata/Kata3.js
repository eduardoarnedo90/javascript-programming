export default function repeatString(stringWord, numberRepeat) {

  let repeat = '';

  if (numberRepeat === 0) {
    return repeat;
  }

  for (let i = 0; i < numberRepeat; i++) {
    repeat += stringWord;
  }

  return repeat;

}