export default function removeFirstAndLast(stringWord) {

  if (stringWord.length <= 2) {
    return stringWord;
  }

  return stringWord.substring(1, stringWord.length - 1);

}