export default function removeSpaces(stringWord) {

  let wordNoSpaces = '';

  for (let i = 0; i < stringWord.length; i++) {
    if (stringWord[i] !== ' ') {
      wordNoSpaces += stringWord[i];
    }
  }

  return wordNoSpaces;

}