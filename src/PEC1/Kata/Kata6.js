function test() {
  console.log(a);
  console.log(foo());
  var a = 1;

  function foo() {
    return 2;
  }
}

test();
/*
  En este caso la funcion foo siempre te va retornar 2 y solo sera visible por test(), ya que es una funcion de clousura.
  En el caso de la variable a, tendra su scope en toda la funcion, des de fuera no se podra consultar.
 */