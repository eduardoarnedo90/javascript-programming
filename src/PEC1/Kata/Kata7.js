const car = {
  brand: 'Ford',
  getBrand() {
    console.log(this.brand);
  }
};

car.getBrand(); //Ford

const cardBrandFunction = car.brand;

console.log(cardBrandFunction);

// He modificado cardBrandFunction() por cardBrandFunction, por que se estava dando valor un valor a
// constante y visualizar en una funcion.
