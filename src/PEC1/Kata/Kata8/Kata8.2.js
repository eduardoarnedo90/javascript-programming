export default function getFruit(tree) {
  if (tree !== null) {
    if (!('fruit' in tree)) {
      return 'No fruit';
    }
    return tree.fruit;
  }
  return null;
};