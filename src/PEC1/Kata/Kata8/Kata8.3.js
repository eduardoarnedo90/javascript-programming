export default class Tree {
  constructor(species, fruit) {
    this.species = species;
    this.fruit = fruit;
  }

  // fruit
  get getFruit() {
    return this.fruit;
  }

};
