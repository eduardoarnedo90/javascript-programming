export default function plantTree(species, fruit) {

  if (typeof species !== 'string' || typeof fruit !== 'string') {
    return null;
  }

  return {
    getSpecies: function getSpecies() {
      return species;
    },
    getFruit: function getFruit() {
      return fruit;
    }
  };

}