export default function plantTree(species, fruit) {

  var _species = species;
  var _fruit = fruit;

  if (typeof _species !== 'string' || typeof _fruit !== 'string') {
    return null;
  }

  return {
    getSpecies: function getSpecies() {
      return _species;
    },
    setSpecies: function setSpecies(value) {
      _species = value;
    },
    getFruit: function getFruit() {
      return _fruit;
    },
    setFruit: function setFruit(value) {
      if (value === 12) {
        throw new Error();
      } else {
        _fruit = value;
      }
    }
  };

}