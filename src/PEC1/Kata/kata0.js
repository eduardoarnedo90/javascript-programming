export default function summation(num) {

  let summation = 0;
  let currentValue = num;

  while (currentValue !== 0) {
    summation += currentValue;
    currentValue--;
  }

  return summation;
}
