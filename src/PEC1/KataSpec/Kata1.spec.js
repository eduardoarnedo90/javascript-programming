import isOdd from '../Kata/Kata1';

describe('Kata #1: Even or Odd', () => {
  test('de 0 es Even', () => {
    expect(isOdd(0)).toBe('Even');
  });
  test('de 1 es Odd', () => {
    expect(isOdd(1)).toBe('Odd');
  });
  test('de 2 es Even', () => {
    expect(isOdd(2)).toBe('Even');
  });
  test('de 3 es Odd', () => {
    expect(isOdd(3)).toBe('Odd');
  });
});