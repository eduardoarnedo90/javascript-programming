import plantTree from '../Kata/Kata10';

describe('Kata #10: Objects', () => {

  test('Capture log tree.presentTree()', () => {
    let tree = new plantTree('pearTree', 'pear');
    const consoleSpy = jest.spyOn(console, 'log');
    console.log(tree.presentTreeFruit('mandarinas'));
    expect(consoleSpy).toHaveBeenCalledWith('Este árbol es un pearTree y da mandarinas');
  });

  test('Capture log tree.presentTree()', () => {
    let tree = new plantTree('pearTree', 'pear');
    const consoleSpy = jest.spyOn(console, 'log');
    console.log(tree.presentTree());
    expect(consoleSpy).toHaveBeenCalledWith('Este árbol es un pearTree y da pear');
  });


});