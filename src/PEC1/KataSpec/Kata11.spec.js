import plantTree from '../Kata/Kata11';

describe('Kata #11: Objects', () => {

  test('tree.setFruit(12) Throw error', () => {
    let tree = new plantTree('pearTree', 'pear');
    expect(() => {
      tree.getFruit(12);
    }).toThrow();
  });

  test('tree.getFruit() to equal "pear"', () => {
    let tree = new plantTree('pearTree', 'pear');
    try {
      tree.setFruit = 12;
    } catch (error) {
      console.log(error);
    }
    expect(tree.getFruit).toBe('pear');
  });

});