import plantTree from '../Kata/Kata11';

describe('Kata #12: Objects', () => {

  test('tree.setFruit("pera") Throw error', () => {
    let tree = new plantTree('manzano', 'manzana');
    expect(() => {
      tree.setFruit('pera');
    }).toThrow();
    expect(tree.getFruit).toBe('manzana');
  });

  test('tree.getFruit("peral") equal to "pera"', () => {
    let tree = new plantTree('peral', 'manzana');
    tree.setFruit = 'pera';
    expect(tree.getFruit).toBe('pera');
  });

});