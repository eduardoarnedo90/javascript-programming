import sumPositiveElements from '../Kata/Kata2';

describe('Kata #2: sumPositiveElements', () => {
  test('The [] is 0', () => {
    expect(sumPositiveElements([])).toBe(0);
  });
  test('The [1,2,3,4,5] is 15', () => {
    expect(sumPositiveElements([1, 2, 3, 4, 5])).toBe(15);
  });
  test('The [1,-2,3,4,5] is 13', () => {
    expect(sumPositiveElements([1, -2, 3, 4, 5])).toBe(13);
  });
  test('The [-1,2,3,4,-5] is 9', () => {
    expect(sumPositiveElements([-1, 2, 3, 4, -5])).toBe(9);
  });
  test('The [-1,-2,-3,-4,-5] is 0', () => {
    expect(sumPositiveElements([-1, -2, -3, -4, -5])).toBe(0);
  });
});