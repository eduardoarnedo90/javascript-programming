import repeatString from '../Kata/Kata3';

describe('Kata #3: repeatString', () => {
  test('The String "JavaScript" and repeat 0 result is ""', () => {
    expect(repeatString('JavaScript', 0)).toBe('');
  });
  test('The String "university" and repeat 1 result is ""', () => {
    expect(repeatString('university', 1)).toBe('university');
  });
  test('The String "hello" and repeat 3 result is ""', () => {
    expect(repeatString('hello', 3)).toBe('hellohellohello');
  });
  test('The String "?" and repeat 10 result is ""', () => {
    expect(repeatString('?', 10)).toBe('??????????');
  });
});