import removeFirstAndLast from '../Kata/Kata4';

describe('Kata #4: removeFirstAndLast', () => {
  test('The String "JavaScript" result is "avaScrip"', () => {
    expect(removeFirstAndLast('JavaScript')).toBe('avaScrip');
  });
  test('The String "Alexandria" result is "lexandri"', () => {
    expect(removeFirstAndLast('Alexandria')).toBe('lexandri');
  });
  test('The String "hydrogen" result is "ydroge"', () => {
    expect(removeFirstAndLast('hydrogen')).toBe('ydroge');
  });
  test('The String "ok" result is "ok"', () => {
    expect(removeFirstAndLast('ok')).toBe('ok');
  });
});