import removeSpaces from '../Kata/Kata5';

describe('Kata #5: removeSpaces', () => {
  test('The String "good morning" result is "goodmorning"', () => {
    expect(removeSpaces('good morning')).toBe('goodmorning');
  });
  test('The String " carrot cake " result is "carrotcake"', () => {
    expect(removeSpaces(' carrot cake ')).toBe('carrotcake');
  });
  test('The String "the abbot gave rice to the fox" result is "theabbotgavericetothefox"', () => {
    expect(removeSpaces('the abbot gave rice to the fox')).toBe('theabbotgavericetothefox');
  });
});