import Tree from '../../Kata/Kata8/Kata8.1';

describe('Kata #8.1: Objects', () => {

  test('The Tree exist species result is True', () => {
    var tree = new Tree('appleTree', 'apple');
    expect('species' in tree).toBe(true);
  });

  test('The Tree species value "appleTree" result is "appleTree"', () => {
    var tree = new Tree('appleTree', 'apple');
    expect(tree.species).toBe('appleTree');
  });

  test('The Tree exist fruit result is True', () => {
    var tree = new Tree('appleTree', 'apple');
    expect('fruit' in tree).toBe(true);
  });

  test('The Tree fruit value "apple" result is "apple"', () => {
    var tree = new Tree('appleTree', 'apple');
    expect(tree.fruit).toBe('apple');
  });

});