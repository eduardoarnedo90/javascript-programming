import getFruit from '../../Kata/Kata8/Kata8.2';

describe('Kata #8.2: Objects', () => {

  test('The Tree not exist result is null', () => {
    expect(getFruit(null)).toBe(null);
  });

  test('The Tree exist, no attributes result is "No fruit"', () => {
    expect(getFruit({})).toBe('No fruit');
  });

  test('The Tree exist, attribute species result is "No fruit"', () => {
    expect(getFruit({ species: 'appleTree' })).toBe('No fruit');
  });

  test('The Tree exist, attribute complete result is "apple"', () => {
    expect(getFruit({ fruit: 'apple', species: 'appleTree' })).toBe('apple');
  });

});