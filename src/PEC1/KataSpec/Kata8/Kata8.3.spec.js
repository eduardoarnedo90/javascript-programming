import Tree from '../../Kata/Kata8/Kata8.3';

describe('Kata #8.3: Objects', () => {

  test('The Tree exist, attribute complete result is "apple"', () => {
    var tree = new Tree('appleTree', 'apple');
    expect(tree.getFruit).toBe('apple');
  });

});