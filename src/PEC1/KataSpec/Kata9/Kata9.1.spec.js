import plantTree from '../../Kata/Kata9/Kata9.1';

describe('Kata #9.1: Factory Objects', () => {

  test('The species is no string and fruit is string, result null', () => {
    expect(plantTree(3, 'pear')).toBe(null);
  });

  test('The species is no string and fruit is string, result null', () => {
    expect(plantTree(true, 'pear')).toBe(null);
  });

  test('The species is string and fruit is no string, result null', () => {
    expect(plantTree('pearTree', 3)).toBe(null);
  });

  test('The species is string and fruit is no string, result null', () => {
    expect(plantTree('pearTree', false)).toBe(null);
  });

  test('The species is string and fruit is string, result {"species":"pearTree", "fruit":"pear"}', () => {
    let tree = plantTree('pearTree', 'pear');
    expect('species' in tree).toBe(true);
    expect('fruit' in tree).toBe(true);
    expect(tree.species).toBe('pearTree');
    expect(tree.fruit).toBe('pear');
    expect(plantTree('pearTree', 'pear')).toStrictEqual({ species: 'pearTree', fruit: 'pear' });
  });

});