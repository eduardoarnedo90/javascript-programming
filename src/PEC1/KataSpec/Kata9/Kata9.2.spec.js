import plantTree from '../../Kata/Kata9/Kata9.2';

describe('Kata #9.2: Factory Objects', () => {

  test('The exist getFruit, result True', () => {
    let tree = plantTree('pearTree', 'pear');
    expect('getFruit' in tree).toBe(true);
  });

  test('The tree.getFruit(), result "pear', () => {
    let tree = plantTree('pearTree', 'pear');
    expect(tree.getFruit()).toBe('pear');
  });

  test('The tree.getFruit() compare tree.fruit, result "pear', () => {
    let tree = plantTree('pearTree', 'pear');
    expect(tree.getFruit()).toBe(tree.fruit);
  });

});