import plantTree from '../../Kata/Kata9/Kata9.3';

describe('Kata #9.3: Factory Objects', () => {

  test('The exist getFruit, result True', () => {
    let tree = plantTree('pearTree', 'pear');
    expect('getFruit' in tree).toBe(true);
  });

  test('The tree.getFruit(), result "pear', () => {
    let tree = plantTree('pearTree', 'pear');
    expect(tree.getFruit()).toBe('pear');
  });

  test('The exist getSpecies, result True', () => {
    let tree = plantTree('pearTree', 'pear');
    expect('getSpecies' in tree).toBe(true);
  });

  test('The tree.getSpecies(), result "pearTree', () => {
    let tree = plantTree('pearTree', 'pear');
    expect(tree.getSpecies()).toBe('pearTree');
  });


});