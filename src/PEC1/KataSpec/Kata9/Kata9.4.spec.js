import plantTree from '../../Kata/Kata9/Kata9.4';

describe('Kata #9.4: Factory Objects', () => {

  test('The exist getFruit, result True', () => {
    let tree = plantTree('pearTree', 'pear');
    expect('getFruit' in tree).toBe(true);
  });

  test('The exist setFruit, result True', () => {
    let tree = plantTree('pearTree', 'pear');
    expect('setFruit' in tree).toBe(true);
  });

  test('The exist getSpecies, result True', () => {
    let tree = plantTree('pearTree', 'pear');
    expect('getSpecies' in tree).toBe(true);
  });

  test('The exist setSpecies, result True', () => {
    let tree = plantTree('pearTree', 'pear');
    expect('setSpecies' in tree).toBe(true);
  });

  test('The change value specie, result "peral"', () => {
    let tree = plantTree('pearTree', 'pear');
    tree.setSpecies('peral');
    expect(tree.getSpecies()).toBe('peral');
  });

  test('The change value fruit, result "pera"', () => {
    let tree = plantTree('pearTree', 'peral');
    tree.setFruit('pera');
    expect(tree.getFruit()).toBe('pera');
  });

});
