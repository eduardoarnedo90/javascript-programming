import summation from '../Kata/kata0';

describe('Kata #0: summation', () => {
  test('de 0 es 0', () => {
    expect(summation(0)).toBe(0);
  });
  test('de 1 es 1', () => {
    expect(summation(1)).toBe(1);
  });
  test('de 2 es 3', () => {
    expect(summation(2)).toBe(3);
  });
  test('de 3 es 6', () => {
    expect(summation(3)).toBe(6);
  });
});