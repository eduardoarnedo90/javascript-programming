export default function evenOdd(num) {
  return (num % 2 === 0) ? 'Par' : 'Impar';
}

// Option B

export function evenOdd2(num) {
  if (num % 2 === 0) {
    return 'Par';
  }
  return 'Impar';
}
