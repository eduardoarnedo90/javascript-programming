export default function sumPositiveElements(numsArray) {
  return numsArray.reduce((acum, number) => (number > 0 ? number + acum : acum), 0);
}

// Option B

export function sumPositiveElements2(numsArray) {
  let acum = 0;

  for (let i = 0; i < numsArray.length; i++) {
    if (numsArray[i] > 0) {
      acum += numsArray[i];
    }
  }

  return acum;
}
