import sumaElementosPositivos from './kata02';

describe('Kata #2: sumPositiveElements', () => {
  test('should return 15 when given [1, 2, 3, 4, 5]', () => {
    expect(sumaElementosPositivos([1, 2, 3, 4, 5])).toBe(15);
  });

  test('should return 13 when given [1, -2, 3, 4, 5]', () => {
    expect(sumaElementosPositivos([1, -2, 3, 4, 5])).toBe(13);
  });

  test('should return 9 when given [-1, 2, 3, 4, -5]', () => {
    expect(sumaElementosPositivos([-1, 2, 3, 4, -5])).toBe(9);
  });

  test('should return 0 when given [-1, -2, -3, -4, -5]', () => {
    expect(sumaElementosPositivos([-1, -2, -3, -4, -5])).toBe(0);
  });
});
