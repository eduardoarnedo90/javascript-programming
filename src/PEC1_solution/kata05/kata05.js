export default function removeSpaces(text) {
  return text.replace(/ /g, '');
}
