import eliminaEspacios from './kata05';

describe('Kata #5: removeSpaces', () => {
  test("de 'buenos días' es 'buenosdías'", () => {
    expect(eliminaEspacios('buenos días')).toBe('buenosdías');
  });

  test("de '   pastel de zanahoria   ' es 'pasteldezanahoria'", () => {
    expect(eliminaEspacios('   pastel de zanahoria   ')).toBe('pasteldezanahoria');
  });

  test("de 'dábale arroz a la zorra el abad' es 'dábalearrozalazorraelabad'", () => {
    expect(eliminaEspacios('dábale arroz a la zorra el abad')).toBe('dábalearrozalazorraelabad');
  });
});
