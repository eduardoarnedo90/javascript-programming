function test() {
  console.log(a);
  console.log(foo());
  var a = 1;
  function foo() {
    return 2;
  }
}
test();

/* Al ejecutar "test()" la respuesta por consola es "undefined" y "2".
El motivo es que el compilador de JS trabaja en dos etapas:
    -Fase 1 (de compilación): Se producen las declaraciones
    -Fase 2 (de ejecución): Se producen las asignaciones
En este punto vamos a reescribir el código para poder ver estas dos partes y,
representando el concepto de "Hoisting" que utiliza el compilador:

    -Fase 1 El compilador, en la fase de compilación, declara las funciones y las variables en
    el Scope de la función "test()"

    */
let a; // Declaración de la variable a
function foo() {
  // Declaración de la función "foo"
  return 2; //* Esta asignación se produce durante la Fase 2 pero, dado que el "Hoisting"
  // se produce "per-scope", esta será su posición de ejecución.
} /*

    -Fase 2 El compilador, en la fase de ejecución, realiza las asignaciones */

console.log(a); // En este momento la variable a todavia no ha sido asignada a ningún valor.
// por ese motivo en consola se imprime "undefined"
console.log(foo()); // Por contra, la función "foo()", gracias al "Hoisting" y a su condición "per-scope"
// devuelve el valor "2" sin problemas

a = 1; // Asignación de la variable durante la ejecución del código (posterior a "console.log(a)")
