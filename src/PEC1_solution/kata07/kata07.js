const car = {
  brand: 'Ford',
  getBrand() {
    console.log(this.brand);
  },
};

car.getBrand(); // Ford

const cardBrandFunction = car.getBrand;

cardBrandFunction(); // undefined

/**
 * Dentro de el metodo de un objeto, la palabra "this"
 * se refiere al propio objeto, en este caso "coche".
 *
 * Cuando asignamos la propiedad "coche.obtenerMarca" a "marcaDelCoche" perdemos
 * la referencia al objeto y por lo tanto "this.marca" retorna "undefined".
 *
 * Para solucionarlo y que la sentencia "marcaDelCoche();" devuelva el contenido de "coche.marca"
 * enemos que contextualizar la propiedad mediante el metodo ".call()".
 *
 * En este caso, le diremos a "marcaDelCoche" que se ejecute en el contexto del objeto "coche"
 * para que la referencia "this" haga referencia al objeto y podamos
 * mostrar por consola la propiedad "marca" del mismo.
 *
 */
cardBrandFunction.call(car); // Ford

/**
 * También funcionaría el método ".bind()"
 * funciona igual pero no ejecuta de forma inmediata sino que deja contextualizada
 * la función para ejcutarse posteriormente */
const cardBrandFunction = car.getBrand.bind(car);
