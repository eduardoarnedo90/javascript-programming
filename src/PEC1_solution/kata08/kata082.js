export default function getFruit(tree) {
  if (tree && tree.hasOwnProperty('fruit')) {
    return tree.fruit;
  }
  return 'No fruit';
}
