import tree from './kata081';
import getFruit from './kata082';

describe('Kata #8.2: funcion getFruit', () => {
  test('should return the value of the fruit when defined', () => {
    expect(getFruit(tree)).toBe('apple');
  });

  test('should return the string "No fruit" when fruit is not defined', () => {
    // Clonar el objeto tree. Equivalente a Object.assign({}, tree);
    const treeWithNoFruit = { ...tree };
    // Eliminar la propiedad fruit del nuevo objeto
    delete treeWithNoFruit.fruit;

    expect(getFruit(treeWithNoFruit)).toBe('No fruit');
  });
});
