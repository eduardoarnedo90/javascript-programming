const tree = {
  species: 'appleTree',
  fruit: 'apple',
  getFruit() {
    return this.fruit || 'No fruit';
  },
};

export default tree;
