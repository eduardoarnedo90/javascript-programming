import plantTree from './kata12';

describe('Kata #12: plantTree()', () => {
  describe('when a tree with values "manzano" y "manzana" is created', () => {
    test('should throw an error when calling .setFruit with "pera"', () => {
      const tree = plantTree('manzano', 'manzana');

      expect(() => {
        tree.setFruit('pera');
      }).toThrow();

      expect(tree.getFruit()).toEqual('manzana');
    });
  });

  describe('when a tree with values "peral" y "manzana" is created', () => {
    test('should not throw an error when calling .setFruit with "pera"', () => {
      const tree = plantTree('peral', 'manzana');

      expect(() => {
        tree.setFruit('pera');
      }).not.toThrow();

      expect(tree.getFruit()).toEqual('pera');
    });
  });
});
