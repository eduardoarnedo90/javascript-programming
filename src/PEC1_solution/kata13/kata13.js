String.prototype.presentarArbol = function (nuestraFruta) {
  let message = `Este árbol es un ${this}`;

  if (nuestraFruta && typeof nuestraFruta === 'string') {
    message = `${message} y da ${nuestraFruta}`;
  }

  console.log(message);
};

export default class Arbol {
  constructor(especie, fruta) {
    if (typeof especie !== 'string' || typeof fruta !== 'string') {
      throw new Error('especie y fruta deben ser strings');
    }

    this._especie = especie;
    this._fruta = fruta;
  }

  get especie() {
    return this._especie;
  }

  get fruta() {
    return this._fruta;
  }

  set especie(valor) {
    if (typeof valor !== 'string') {
      throw new Error('especie debe ser un string');
    }

    if (!this._fruta.match(`^[${valor}]{4}`)) {
      throw new Error(`${valor} no valida con la especie ${this._especie}.`);
    }

    this._especie = valor;
  }

  set fruta(valor) {
    if (typeof valor !== 'string') {
      throw new Error();
    }

    if (!this._especie.match(`^[${valor}]{4}`)) {
      throw new Error(`${valor} no valida con la fruta ${this._fruta}.`);
    }

    this._fruta = valor;
  }
}
