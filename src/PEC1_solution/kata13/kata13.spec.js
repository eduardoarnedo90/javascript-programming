import Arbol from './kata13';

describe('Kata #13: clase Arbol', () => {
  global.console = {
    log: jest.fn(),
  };

  test('solo se instancia con (string, string)', () => {
    expect(() => new Arbol(1, 1)).toThrow();
    expect(() => new Arbol('peral', 1)).toThrow();
    expect(() => new Arbol(1, 'pera')).toThrow();
    expect(() => new Arbol(undefined, undefined)).toThrow();
    expect(() => new Arbol(null, null)).toThrow();
  });

  test('tiene getters y setters para fruta y especie', () => {
    const peral = new Arbol('peral', 'pera');

    expect(peral).toHaveProperty('fruta');
    expect(peral).toHaveProperty('especie');
  });

  test('el getter especie devuelve un string con un metodo llamado presentarArbol', () => {
    const peral = new Arbol('peral', 'pera');

    expect(typeof peral.especie).toBe('string');
    expect(typeof peral.especie.presentarArbol).toBe('function');
  });

  test("ejecutar presentarArbol() escribe en consola 'Este árbol es un peral'", () => {
    const peral = new Arbol('peral', 'pera');

    peral.especie.presentarArbol();

    expect(console.log).toHaveBeenCalledWith('Este árbol es un peral');
  });

  test("ejecutar presentarArbol('peras') escribe en consola 'Este árbol es un peral y da peras'", () => {
    const peral = new Arbol('peral', 'pera');

    peral.especie.presentarArbol('peras');

    expect(console.log).toHaveBeenCalledWith('Este árbol es un peral y da peras');
  });

  test('el setter especie con valor 12 lanza error y no cambia valor', () => {
    const arbol = new Arbol('peral', 'pera');

    expect(() => {
      arbol.especie = 12;
    }).toThrow();
    expect(arbol.especie).toBe('peral');
  });

  test('con arbol(manzano, manzana), al definir fruta como pera, lanza error y mantiene valor manzana', () => {
    const manzano = new Arbol('manzano', 'manzana');

    expect(() => {
      manzano.fruta = 'pera';
    }).toThrow();
    expect(manzano.fruta).toBe('manzana');
  });

  test('con arbol(peral, manzana), al definir fruta como pera, modifica valor a pera', () => {
    const peral = new Arbol('peral', 'manzana');

    peral.fruta = 'pera';

    expect(peral.fruta).toBe('pera');
  });
});
