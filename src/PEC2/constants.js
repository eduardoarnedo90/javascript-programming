
export const ENDPOINTS = Object.freeze({
  MOVIES: '/api/films/',
  PERSONS: '/api/people/'
});

export const FIELDS = Object.freeze({
  COUNT: 'count',
  RESULTS : 'results',
  TITLE : 'title',
  DIRECTOR : 'director',
  RELEASE_DATE : 'release_date',
  EPISODE_ID : 'episode_id',
  CHARACTERS : 'characters',
  NAME : 'name',
  HOMEWORLD : 'homeworld'
});