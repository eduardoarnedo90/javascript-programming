import { ENDPOINTS, FIELDS } from './constants';
import fetch from 'cross-fetch';

const uri = 'https://swapi.dev';

// API calls

/**
 * Return all movies
 * @returns {Promise<*|void>}
 */
async function getMoviesAPI() {
  let promise = new Promise((resolve, reject) => {
    resolve(fetch(uri.concat(ENDPOINTS.MOVIES))).catch(function(error) {
      reject(new Error(error))
    });
  });
  return await promise.then(res => {
    if (res.status === 200) {
      return res.json();
    }
  }, reason => {
    throw new Error(reason);
  });
}

/**
 * Return movie of id
 * @param id
 * @returns {Promise<*|void>}
 */
async function getMovieIdAPI(id) {
  if (typeof id !== 'string') {
    throw new Error('id they have to be strings');
  }
  let promise = new Promise((resolve, reject) => {
    resolve(fetch(uri.concat(ENDPOINTS.MOVIES).concat(id))).catch(function(error) {
      reject(new Error(error))
    });
  });
  return await promise.then(res => {
    if (res.status === 200) {
      return res.json();
    }
  }, reason => {
    throw new Error(reason);
  });
}

/**
 * Return person of Id
 * @param url
 * @returns {Promise<*|void>}
 */
async function getPersonId(url) {
  url.replace('http://', 'https://');
  let promise = new Promise((resolve, reject) => {
    resolve(fetch(url)).catch(function(error) {
      reject(new Error(error))
    });
  });
  return await promise.then(res => {
    if (res.status === 200) {
      return res.json();
    }
  }, reason => {
    throw new Error(reason);
  });
}

/**
 * Return planet of Id
 * @param url
 * @returns {Promise<*|void>}
 */
async function getPlanetId(url) {
  url.replace('http://', 'https://');
  let promise = new Promise((resolve, reject) => {
    resolve(fetch(url)).catch(function(error) {
      reject(new Error(error))
    });
  });
  return await promise.then(res => {
    if (res.status === 200) {
      return res.json();
    }
  }, reason => {
    throw new Error(reason);
  });
}

// Methods Practice Stars Wards

/**
 * Return number of movies in the API
 * @returns {Promise<int>}
 */
async function getMovieCount() {
  let promise = new Promise((resolve, reject) => {
    resolve(getMoviesAPI()).catch(function(error) {
      reject(new Error(error))
    });
  });
  return await promise.then(data => {
    return data[FIELDS.COUNT];
  }, reason => {
    console.log(reason)
    throw new Error(reason);
  });
}

/**
 * Return json array contains movie info(name, director, episodeID, release)
 * @returns {Promise<[]>}
 */
async function listMovies() {
  let promise = new Promise((resolve, reject) => {
    resolve(getMoviesAPI()).catch(function(error) {
      reject(new Error(error))
    });
  });
  return await promise.then(data => {
    let listFilm = [];
    for (let i = 0; i < data[FIELDS.RESULTS].length; i++) {
      listFilm.push(
        {
          'name': data[FIELDS.RESULTS][i][FIELDS.TITLE],
          'director': data[FIELDS.RESULTS][i][FIELDS.DIRECTOR],
          'release': data[FIELDS.RESULTS][i][FIELDS.RELEASE_DATE],
          'episodeID': data[FIELDS.RESULTS][i][FIELDS.EPISODE_ID]
        }
      );
    }
    return listFilm;
  }, reason => {
    throw new Error(reason);
  });
}

/**
 * Return json array contains movies sorted for name
 * @returns {Promise<this>}
 */
async function listMoviesSorted() {
  let promise = new Promise((resolve, reject) => {
    resolve(listMovies()).catch(function(error) {
      reject(new Error(error))
    });
  });
  return promise.then(data => {
    return data.sort((a, b) => (a.name > b.name) ? 1 : -1);
  }, reason => {
    throw new Error(reason);
  });
}

/**
 * Return json array contains movies sorted for name and filter for episodeId
 * @returns {Promise<* | void>}
 */
async function listEvenMoviesSorted() {
  let promise = new Promise((resolve, reject) => {
    resolve(listMovies()).catch(function(error) {
      reject(new Error(error))
    });
  });
  return promise.then(data => {
    return data.sort((a, b) => (a.episodeID > b.episodeID) ? 1 : -1).filter(function(data) {
      return (data.episodeID % 2) === 0;
    });
  }, reason => {
    throw new Error(reason);
  });
}

/**
 * Return json movie info (name, episodeID, characters)
 * @param id
 * @returns {Promise<{characters: *, name: *, episodeID: *} | void>}
 */
async function getMovieInfo(id) {
  let promise = new Promise((resolve, reject) => {
    resolve(getMovieIdAPI(id)).catch(function(error) {
      reject(new Error(error))
    });
  });
  return promise.then(data => {
    return {
      'name': data[FIELDS.TITLE],
      'episodeID': data[FIELDS.EPISODE_ID],
      'characters': data[FIELDS.CHARACTERS]
    };
  }, reason => {
    throw new Error(reason);
  });
}

/**
 * Return json movie info, and array of characters contains name person.
 * @param id
 * @returns {Promise<{characters: *, name: *, episodeID: *}|void>}
 */
async function getMovieCharacters(id) {
  let data = await getMovieInfo(id);
  data.characters = await getNamePersonPromise(data.characters);
  return data;
}

/**
 * Return json movie info, and array of characters contains name and planet of person
 * @param id
 * @returns {Promise<{characters: *, name: *, episodeID: *}|void>}
 */
async function getMovieCharactersAndHomeworlds(id) {
  let data = await getMovieInfo(id);
  data.characters = await getNameHomeworlPersonPromise(data.characters);
  return data;
}

/**
 * Return Object Movie
 * @param id
 * @returns {Promise<Movie>}
 */
async function createMovie(id) {
  const movie = await getMovieInfo(id);
  return new Movie(movie.name, movie.characters);
}

//Classes

class Movie {
  constructor(name, characters) {
    this.name = name;
    this.characters = characters;
  }

  /**
   * Return array of urls person
   * @returns {Promise<ValidationOptions.unknown[]|void>}
   */
  async getCharacters() {
    let auxCharacters = this.characters;
    return await getNamePersonPromise(auxCharacters);
  }

  /**
   * Return array content name and planet of person
   * @returns {Promise<ValidationOptions.unknown[]|void>}
   */
  async getHomeworlds() {
    let auxCharacters = this.characters;
    return await getNameHomeworlPersonPromise(auxCharacters);
  }

  /**
   * Return array content name and planet of person sorted
   * @returns {Promise<*|void>}
   */
  async getHomeworldsReverse() {
    let auxCharacters = await this.getHomeworlds();
    let promise = new Promise((resolve, reject) => {
      resolve(auxCharacters).catch(function(error) {
        reject(new Error(error))
      });
    });
    return await promise.then(data => {
      return data.sort((a, b) => (a[FIELDS.HOMEWORLD] > b[FIELDS.HOMEWORLD]) ? 1 : -1);
    }, reason => {
      throw new Error(reason);
    });
  }

}

// Other methods

/**
 * Return array content name of person.
 * @param characters
 * @returns {Promise<unknown[] | void>}
 */
async function getNamePersonPromise(characters) {
  let auxCharacters = characters;
  return Promise.all(characters).then(characters => {
    for (let i = 0; i < characters.length; i++) {
      let promiseURL = new Promise((resolve, reject) => {
        resolve(getPersonId(characters[i])).catch(function(error) {
          reject(new Error(error))
        });
      });
      promiseURL.then(person => {
        auxCharacters[i] = person[FIELDS.NAME];
      });
    }
    return auxCharacters;
  }, reason => {
    throw new Error(reason);
  });
}

/**
 * Return array content name and planet of person
 * @param characters
 * @returns {Promise<unknown[] | void>}
 */
async function getNameHomeworlPersonPromise(characters) {
  let auxCharacters = characters;
  return Promise.all(characters).then(characters => {
    for (let i = 0; i < characters.length; i++) {
      let promiseName = new Promise((resolve, reject) => {
        resolve(getPersonId(characters[i])).catch(function(error) {
          reject(new Error(error))
        });
      });
      promiseName.then(person => {
        auxCharacters[i] = {
          'name': '',
          'homeworld': ''
        };
        auxCharacters[i][FIELDS.NAME] = person[FIELDS.NAME];
        let promisePlanet = new Promise((resolve, reject) => {
          resolve(getPlanetId(person[FIELDS.HOMEWORLD])).catch(function(error) {
            reject(new Error(error))
          });
        });
        promisePlanet.then(planet => {
          auxCharacters[i][FIELDS.HOMEWORLD] = planet[FIELDS.NAME];
        });
      });
    }
    return auxCharacters;
  }, reason => {
    throw new Error(reason);
  });
}

export {
  getMovieCount,
  listMovies,
  listMoviesSorted,
  listEvenMoviesSorted,
  getMovieInfo,
  getMovieCharacters,
  getMovieCharactersAndHomeworlds,
  createMovie,
  Movie
};