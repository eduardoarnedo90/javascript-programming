import {
  createMovie,
  getMovieCharacters,
  getMovieCharactersAndHomeworlds,
  getMovieCount,
  getMovieInfo,
  listEvenMoviesSorted,
  listMovies,
  listMoviesSorted
} from './pec2';

describe('Test1: getMovieCount', () => {
  test('should return "6" when given API call"', () => {
    getMovieCount().then(res => {
      expect(res).toBe(6);
    });
  });
});

describe('Test2: listMovies', () => {
  test('should return "[{...}, ..., {....}]" when given API call"', () => {
    listMovies().then(res => {
      let expected = [];
      expected.push({ name: 'A New Hope', director: 'George Lucas', release: '1977-05-25', episodeID: 4 });
      expected.push({
        name: 'The Empire Strikes Back',
        director: 'Irvin Kershner',
        release: '1980-05-17',
        episodeID: 5
      });
      expected.push({ name: 'Return of the Jedi', director: 'Richard Marquand', release: '1983-05-25', episodeID: 6 });
      expected.push({ name: 'The Phantom Menace', director: 'George Lucas', release: '1999-05-19', episodeID: 1 });
      expected.push({ name: 'Attack of the Clones', director: 'George Lucas', release: '2002-05-16', episodeID: 2 });
      expected.push({ name: 'Revenge of the Sith', director: 'George Lucas', release: '2005-05-19', episodeID: 3 });
      expect(res).toBe(expected);
      expect(res.length).toBe(expected.length);
    });
  });
});

describe('Test3: listMoviesSorted', () => {
  test('should return "[{...}, ..., {....}]" when given API call"', () => {
    listMoviesSorted().then(res => {
      let expected = [];
      expected.push({ name: 'A New Hope', director: 'George Lucas', release: '1977-05-25', episodeID: 4 });
      expected.push({ name: 'Attack of the Clones', director: 'George Lucas', release: '2002-05-16', episodeID: 2 });
      expected.push({ name: 'Return of the Jedi', director: 'Richard Marquand', release: '1983-05-25', episodeID: 6 });
      expected.push({ name: 'Revenge of the Sith', director: 'George Lucas', release: '2005-05-19', episodeID: 3 });
      expected.push({
        name: 'The Empire Strikes Back',
        director: 'Irvin Kershner',
        release: '1980-05-17',
        episodeID: 5
      });
      expected.push({ name: 'The Phantom Menace', director: 'George Lucas', release: '1999-05-19', episodeID: 1 });
      expect(res).toBe(expected);
      expect(res.length).toBe(expected.length);
    });
  });
});

describe('Test4: listEvenMoviesSorted', () => {
  test('should return "[{...}, ..., {....}]" when given API call"', () => {
    listEvenMoviesSorted().then(res => {
      let expected = [];
      expected.push({ name: 'Attack of the Clones', director: 'George Lucas', release: '2002-05-16', episodeID: 2 });
      expected.push({ name: 'A New Hope', director: 'George Lucas', release: '1977-05-25', episodeID: 4 });
      expected.push({ name: 'Return of the Jedi', director: 'Richard Marquand', release: '1983-05-25', episodeID: 6 });
      expect(res).toBe(expected);
      expect(res.length).toBe(expected.length);
    });
  });
});

describe('Test5: getMovieInfo', () => {
  test('should return "{...}" when given API call"', () => {
    getMovieInfo('4').then(res => {
      let movie = { name: 'A New Hope', episodeID: 4, characters: Array(18) };
      expect(res).toBe(movie);
      expect(res.characters.length).toBe(movie.characters.length);
    });
  });
  test('should return "{...}" when given API call"', () => {
    getMovieInfo('1').then(res => {
      let movie = { name: 'The Phantom Menace', episodeID: 1, characters: Array(34) };
      expect(res).toBe(movie);
      expect(res.characters.length).toBe(movie.characters.length);
    });
  });
});

describe('Test6: getMovieCharacters', () => {
  test('should return "{...}" when given API call"', () => {
    getMovieCharacters('1').then(res => {
      let movie = { name: 'A New Hope', episodeID: 4, characters: Array(18) };
      expect(res).toBe(movie);
      expect(res.characters.length).toBe(movie.characters.length);
    });
  });
  test('should return "{...}" when given API call"', () => {
    getMovieCharacters('2').then(res => {
      let movie = { name: 'The Empire Strikes Back', episodeID: 5, characters: Array(16) };
      expect(res).toBe(movie);
      expect(res.characters.length).toBe(movie.characters.length);
    });
  });
});

describe('Test7: getMovieCharactersAndHomeworlds', () => {
  test('should return "{...}" when given API call"', () => {
    getMovieCharactersAndHomeworlds('1').then(res => {
      let movie = { name: 'A New Hope', episodeID: 4, characters: Array(18) };
      expect(res).toBe(movie);
      expect(res.characters.length).toBe(movie.characters.length);
    });
  });
});

describe('Test8: createMovie', () => {
  test('should return "[...]" when given API call"', () => {
    createMovie('1').then(res => {
      res.getCharacters().then(listCharacters => {
        let listExpected = [
          'http://swapi.dev/api/people/1/',
          'http://swapi.dev/api/people/2/',
          'http://swapi.dev/api/people/3/',
          'http://swapi.dev/api/people/4/',
          'http://swapi.dev/api/people/5/',
          'http://swapi.dev/api/people/6/',
          'http://swapi.dev/api/people/7/',
          'http://swapi.dev/api/people/8/',
          'http://swapi.dev/api/people/9/',
          'http://swapi.dev/api/people/10/',
          'http://swapi.dev/api/people/12/',
          'http://swapi.dev/api/people/13/',
          'http://swapi.dev/api/people/14/',
          'http://swapi.dev/api/people/15/',
          'http://swapi.dev/api/people/16/',
          'http://swapi.dev/api/people/18/',
          'http://swapi.dev/api/people/19/',
          'http://swapi.dev/api/people/81/'
        ];
        expect(listCharacters).toBe(listExpected);
        expect(listCharacters.length).toBe(listExpected.length);

      });
    });
  });
  test('should return "[...]" when given API call"', () => {
    createMovie('1').then(res => {
      res.getHomeworlds().then(listCharacters => {
        let listExpected = [
          { name: 'Luke Skywalker', homeworld: 'Tatooine' },
          { name: 'C-3PO', homeworld: 'Tatooine' },
          { name: 'R2-D2', homeworld: 'Naboo' },
          { name: 'Darth Vader', homeworld: 'Tatooine' },
          { name: 'Leia Organa', homeworld: 'Alderaan' },
          { name: 'Owen Lars', homeworld: 'Tatooine' },
          { name: 'Beru Whitesun lars', homeworld: 'Tatooine' },
          { name: 'R5-D4', homeworld: 'Tatooine' },
          { name: 'Biggs Darklighter', homeworld: 'Tatooine' },
          { name: 'Obi-Wan Kenobi', homeworld: 'Stewjon' },
          { name: 'Wilhuff Tarkin', homeworld: 'Eriadu' },
          { name: 'Chewbacca', homeworld: 'Kashyyyk' },
          { name: 'Han Solo', homeworld: 'Corellia' },
          { name: 'Greedo', homeworld: 'Rodia' },
          { name: 'Jabba Desilijic Tiure', homeworld: 'Nal Hutta' },
          { name: 'Wedge Antilles', homeworld: 'Corellia' },
          { name: 'Jek Tono Porkins', homeworld: 'Bestine IV' },
          { name: 'Raymus Antilles', homeworld: 'Alderaan' }
        ];
        expect(listCharacters).toBe(listExpected);
        expect(listCharacters.length).toBe(listExpected.length);
      });
    });
  });
});